import QtQuick.Controls 2.2
import QtQuick 2.9
import Ubuntu.Components 1.3

ToolTip {
    id: notifyTooltip
    
    property real oskPadding
    
    background: Rectangle {color: theme.palette.normal.base; radius: units.gu(1)}
    font.pixelSize: units.gu(2)
    padding: units.gu(1)
    
    function display(customText, position, customTimeout){
        switch(position){
            case "TOP":
                y = units.gu(5);
            break;
            case "BOTTOM":
                y = root.height - units.gu(10) - oskPadding;
            break;
            default:
                y = 10;
            break;
        }
        
        if(customTimeout){
            timeout = customTimeout
        }
        
        text = customText
        
        visible = true
    }
    
    timeout: 3000
}
