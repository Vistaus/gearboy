import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QQC2
import QtQuick.Controls.Suru 2.2
import "components"

Page {
    id: inputPage
    anchors.fill: parent
    
    
    /****** Functions *****/
    
    function deleteInput(index){
        var temp = gameSettings.externalInputs.slice()
            
        temp.splice(index, 1)
        gameSettings.externalInputs = temp.slice()
    }
    
    function showInputPage(editMode, data) {
        
        if (editMode) {
            externalInputPage.title = i18n.tr("Edit input")
            externalInputPage.inputData = data
        } else {
            externalInputPage.title = i18n.tr("Add new input")
            externalInputPage.inputData = {"name": "", "type" : "keyboard", "keymap" : {
                    "up": "-1",
                    "down": "-1",
                    "left": "-1",
                    "right": "-1",
                    "a": "-1",
                    "b": "-1",
                    "select": "-1",
                    "start": "-1",
                    "pause": "-1",
                    "quicksave": "-1",
                    "quickload": "-1",
                    "fast": "-1",
                    "mute": "-1",
                    "prevslot": "-1",
                    "nextslot": "-1"
                  }}
        }
                            
        externalInputPage.editMode = editMode
        externalInputPage.visible = true
    }

    Rectangle {
        id: bkg
        anchors.fill: parent
        color: Suru.backgroundColor
    }

    header: PageHeader {
        title: i18n.tr("Input settings")

        leadingActionBar.actions: [
            Action {
                iconName: 'back'
                text: i18n.tr("Back")
                onTriggered: inputPage.visible = false
            }
        ]
        
        trailingActionBar.actions: [
            Action {
                iconName: 'add'
                text: i18n.tr("Add")
                onTriggered: {
                    showInputPage(false)
                }
            }
        ]

        flickable: settingsListView
    }
    
     ScrollView {
         id: scrollView
         
         anchors.fill: parent

        Flickable {
            id: settingsListView

            interactive: true
            clip: true
            contentHeight: settingsColumn.height
            anchors.fill: parent
            
            Column {
                id: settingsColumn

                anchors {
                    top: parent.top
                    left: parent.left
                    right: parent.right
                }

                ItemHeader {
                    title: i18n.tr("On-screen input")
                }
                
                SwitchItem {
                    id: touchControls
                    
                    titleText.text: i18n.tr("Enable on-screen input")
                    checked: gameSettings.enableTouch
                    
                    onCheckedChanged: {
                        gameSettings.enableTouch = checked
                        if (hideAllUICheckedBox) {
                            root.showUI = false
                        }
                    }
                }
                
                CheckBoxItem {
                    id: hideAllUICheckedBox
                    
                    visible: !touchControls.checked
                    titleText.text: i18n.tr("Hide all on-screen buttons")
                    subText.text: i18n.tr("Tap screen to toggle visibility of the buttons")
                    bindValue: gameSettings.hideOtherUI
                    onCheckboxValueChanged: {
                        gameSettings.hideOtherUI = checkboxValue
                        root.showUI = false
                    }
                }
                
                CheckBoxItem {
                    id: autoHideTouchCheckedBox
                    
                    titleText.text: i18n.tr("Automatically disable on-screen buttons")
                    subText.text: i18n.tr("When external input is detected")
                    bindValue: gameSettings.autoHideTouch
                    onCheckboxValueChanged: {
                        gameSettings.autoHideTouch = checkboxValue
                    }
                }
                
                ListItem {
                    
                    height: units.gu(12)
                    visible: touchControls.checked
                
                    Column {
                        
                        anchors {
                            left: parent.left
                            right: parent.right
                            margins: units.gu(2)
                            verticalCenter: parent.verticalCenter
                        }
                        
                        spacing: units.gu(2)
                    
                        Label {
                            id: opacityLabel
                            text: i18n.tr("Opacity")
                            
                            anchors {
                                left: parent.left
                                right: parent.right
                            }
                        }
                        
                        Slider {
                            id: slider
                            
                            function formatValue(v) { return v.toFixed(2) }
                            minimumValue: 0.2
                            maximumValue: 1.0
                            value: gameSettings.touchOpacity
                            live: false

                            
                            anchors {
                                left: parent.left
                                right: parent.right
                                margins: units.gu(2)
                            }
                            
                            onValueChanged: gameSettings.touchOpacity = value
                        }
                    }
                }
                
                ItemHeader {
                    title: i18n.tr("External input")
                }
                
                CheckBoxItem {
                    id: speedCheckbox
                    
                    titleText.text: i18n.tr("Fast forward only while key is pressed")
                    subText.text: i18n.tr("Otherwise, it works like a toggle")
                    bindValue: gameSettings.momentarySwitchSpeed
                    onCheckboxValueChanged: {
                        gameSettings.momentarySwitchSpeed = checkboxValue
                    }
                }
    
                ListView {
                    id: listView

                    currentIndex: -1
                    interactive: false
                    height: units.gu(7) * count
                    
                    anchors {
                        left: parent.left
                        right: parent.right
                    }
                    
                    model: gameSettings.externalInputs
                    
                    delegate: ListItem{
                        id: listItem
                        
                        selected: gameSettings.selectedInput === modelData.name
                        
                        onClicked: gameSettings.selectedInput = modelData.name
                        
                        ListItemLayout {
                            title.text: modelData.name
                            
                            Icon {
                                id: leadingIcon

                                implicitWidth: units.gu(3)
                                implicitHeight: implicitWidth
                                name: switch (modelData.type) {
                                        case "keyboard":
                                            "input-keyboard-symbolic"
                                            break;
                                        case "gamepad":
                                            "input-gaming-symbolic"
                                            break
                                        default:
                                            "other-actions"
                                            break
                                    }
                                color: selected ? theme.palette.selected.backgroundText : theme.palette.normal.backgroundText
                                
                                SlotsLayout.position: SlotsLayout.Leading
                             }
                            
                            Icon {
                                id: trailingIcon
                                
                                visible: listItem.selected
                                implicitWidth: units.gu(3)
                                implicitHeight: implicitWidth
                                name: "ok"
                                
                                SlotsLayout.position: SlotsLayout.Trailing
                             }
                        }
                         
                        leadingActions: ListItemActions {
                            id: leading
                            actions: Action {
                                visible: !listItem.selected
                                iconName: "delete"
                                text: i18n.tr("Delete")
                                shortcut: StandardKey.Delete
                                onTriggered: {
                                    inputPage.deleteInput(index)
                                }
                            }
                        }

                        trailingActions: ListItemActions {
                            id: trailing
                            actions: [
                                Action {
                                    iconName: "settings"
                                    text: i18n.tr("Settings")
                                    
                                    onTriggered: {
                                        showInputPage(true, modelData)
                                    }
                                }
                            ]
                        }
                    }
                }
            }
        }
    }
}
