import QtQuick 2.9
import Ubuntu.Components 1.3
import QtQuick.Controls 2.2 as QQC2
import "components"

QQC2.Drawer {
    id: bottomEdge
    
    height: units.gu(12)
    width: parent.width
    background: Rectangle {color: theme.palette.normal.background}
    edge: Qt.BottomEdge
    focus: false
    
    Row {
        
        anchors.centerIn: parent
        spacing: units.gu(2)
        
        RoundButton {
            id: fullscreenButton
            
            property bool disabled: !gameSettings.fullscreen

            buttonType: disabled ? "neutral" : "positive"
            iconName: disabled ? "view-restore" : "view-fullscreen"
            width: units.gu(8)
            height: width
            anchors.verticalCenter: parent.verticalCenter

            onClicked: {
                gameSettings.fullscreen = !gameSettings.fullscreen
            }
        }
        
        RoundButton {
            id: mutedButton
            
            property bool muted: !gameSettings.sound

            buttonType: muted ? "neutral" : "positive"
            iconName: muted ? "audio-volume-muted" : "audio-volume-high"
            width: units.gu(8)
            height: width
            anchors.verticalCenter: parent.verticalCenter

            onClicked: {
                root.toggleSound(false)
            }
        }
        
        RoundButton {
            id: touchControlsButton
            
            property bool disabled: !gameSettings.enableTouch

            buttonType: disabled ? "neutral" : "positive"
            iconName: disabled ? "input-dialpad-hidden-symbolic" : "input-dialpad-symbolic"
            width: units.gu(8)
            height: width
            anchors.verticalCenter: parent.verticalCenter

            onClicked: {
                gameSettings.enableTouch = !gameSettings.enableTouch
            }
        }
        
        RoundButton {
            id: fastButton
            
            property bool disabled: !root.fastSpeed

            visible: emu.isRunning
            buttonType: disabled ? "neutral" : "positive"
            iconName: "media-seek-forward"
            width: units.gu(8)
            height: width
            anchors.verticalCenter: parent.verticalCenter

            onClicked: {
                disabled = !disabled
                root.toggleSpeed()
            }
        }
    }
}
