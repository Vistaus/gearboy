#ifndef FILES_H
#define FILES_H

#include <QObject>

class Files: public QObject {
    Q_OBJECT

    Q_PROPERTY(QString romDir READ romDir CONSTANT)
    Q_PROPERTY(QString savesDir READ savesDir CONSTANT)
    Q_PROPERTY(QString settingsDir READ settingsDir CONSTANT)

public:
    Files(QObject* parent = 0);
    ~Files();

    static Files *instance(QObject* parent = 0) {
        if (!m_files) {
            m_files = new Files(parent);
        }
        return m_files;
    }

    QString romDir();
    QString savesDir();
    QString settingsDir();
    Q_INVOKABLE QString moveRom(const QString path);
    Q_INVOKABLE void removeRom(const QString path);
    Q_INVOKABLE QString readCheats(const QString gamePath);
    Q_INVOKABLE void writeCheats(const QString gamePath, const QString cheats);

private:
    QString cheatPath(const QString gamePath);

    static Files *m_files;
};

#endif
